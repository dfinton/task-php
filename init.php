<?php

include 'config.php';
include 'lib.php';

$mysqli = connect($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);

$dropTaskTableQuery = "
    DROP TABLE IF EXISTS `task`;
";

$createTaskTableQuery = "
    CREATE TABLE `task` (
        `id` int(11) unsigned NOT NULL auto_increment,
        `name` VARCHAR(255) NOT NULL default '',
        `date` DATETIME NOT NULL default NOW(),
        `interval` ENUM('hour', 'day', 'week', 'month', 'year') NOT NULL default 'hour',
        PRIMARY KEY(`id`)
    );
";

$seedTaskTableQuery = "
    INSERT INTO `task` (`name`, `date`, `interval`)
    VALUES 
        ('Test 1', NOW(), 'month'),
        ('Test 2', NOW(), 'day'),
        ('Test 3', NOW(), 'week'),
        ('Test 4', NOW(), 'year'),
        ('Test 5', NOW(), 'hour');
";

query($mysqli, $dropTaskTableQuery);
query($mysqli, $createTaskTableQuery);
query($mysqli, $seedTaskTableQuery);
