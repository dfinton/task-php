<!DOCTYPE html>
<html lang="en" ng-app="TaskApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <script src="js/angular.min.js"></script>
        <script src="js/app.js"></script>

        <title>Raw PHP Task Manager Assessment</title>
    </head>

    <body ng-controller="TaskController">
        <div class="container">
            <nav class="navbar navbar-default centered">
                <h1>Raw PHP Task Manager Assessment</h1>
            </nav>

            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Date and Time</th>
                                    <th>Due Date</th>
                                    <th>Interval</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="task in taskList">
                                    <td>{{ task.id }}</td>
                                    <td>{{ task.name }}</td>
                                    <td>{{ task.date }}</td>
                                    <td>{{ task.dueDate }}</td>
                                    <td>1 {{ task.interval }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-lg-12 centered">
                    <input ng-click="event.showCreateModal()" type="button" class="btn btn-primary create-task-show" value="Create New Task"></input>
                </div>
            </div>
        </div>

        <!-- Error Modal -->
        <div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="error-modal-label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                        <h4 class="modal-title" id="error-modal-label">There has been an error</h4>
                    </div>
                    <div class="modal-body" id="error-modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <span id="save-task-button"></span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Edit Modal -->
        <div class="modal fade" id="edit-task-modal" tabindex="-1" role="dialog" aria-labelledby="edit-task-modal-label">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                        <h4 class="modal-title" id="edit-task-modal-label">Create New Task</h4>
                    </div>
                    <div class="modal-body" id="edit-task-modal-body">
                        <form id="edit-task-form" class="form-horizontal">
                            <div class="input-group padded">
                                <label for="task-name" class="control-label">Description</label>
                                <input type="text" class="form-control" id="task-name" name="task-name" placeholder="Enter Name for Task"></input>
                            </div>
                            <div>
                                <label for="task-date" class="control-label">Date</label>
                                <div class='input-group date padded' id='task-date-time-picker'>
                                    <input type='text' class="form-control" id="task-date" name="task-date"></input>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div>
                                <label for="task-interval" class="control-label">Interval</label>
                                <div class="input-group padded">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default active">
                                            <input type="radio" value="hour" name="task-interval" class="task-interval" checked="checked"></input>
                                            Hour
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" value="day" name="task-interval" class="task-interval"></input>
                                            Day
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" value="week" name="task-interval" class="task-interval"></input>
                                            Week
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" value="month" name="task-interval" class="task-interval"></input>
                                            Month
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" value="year" name="task-interval" class="task-interval"></input>
                                            Year
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button ng-click="event.createTask()" type="button" class="btn btn-primary create-task-save">Create Task</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <span id="save-task-button"></span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
