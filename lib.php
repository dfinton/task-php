<?php

/**
 * Helper function to connect to the MysQL database. Returns an error and exits
 * the script if there's any issues
 *
 * @param string $dbHost
 * @param string $dbUser
 * @param string $dbPassowrd
 * @param string $dbName
 * @param integer $dbPort
 * @return mysqli
 */
function connect($dbHost, $dbUser, $dbPassword, $dbName, $dbPort) {
    $mysqli = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);

    if ($mysqli->connect_error) {
        $errno = $mysqli->connect_errno;
        $error = $mysqli->connect_error;

        echo "There was a problem connecting to the database. The error number was {$errno} and the error was:\n\n";
        echo "{$error}\n\n";

        exit();
    }

    return $mysqli;
}

/**
 * Helper function to query the database and returns the results. Prints
 * an error and exists the script if there are any issues
 *
 * @param mysqli $mysqli
 * @param string $statement
 * @return mysqli_result
 */
function query($mysqli, $statement) {
    $result = $mysqli->query($statement);

    if ($mysqli->error) {
        $errno = $mysqli->errno;
        $error = $mysqli->error;

        echo "There was a problem running the following SQL command:\n\n";
        echo "{$statement}\n\n";
        echo "The error number was {$errno}, and the error was:\n\n";
        echo "{$error}\n\n";

        exit();
    }

    return $result;
}

/**
 * Helper function that takes a data structure and returns to the requester
 * the JSONified representation of that data. This function will always exit
 * the script
 *
 * @param array $data
 * @return null
 */
function json($data) {
    $jsonData = json_encode($data);

    header('Content-Type: application/json');
    echo $jsonData;
    exit();
}

/**
 * Helper function that reports an error message in JSON format
 *
 * @param string $data
 * @return null
 */
function error($message) {
    $errorResult = array(
        'error' => true,
        'message' => $message,
    );

    json($errorResult);
}
