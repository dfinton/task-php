/**
 *  Angular Services
 */
var TaskService = angular.module('TaskService', []);

TaskService.factory('TaskServiceOp', ['$http', function($http) {
    var taskListUrl = "getTask.php";
    var createTaskUrl = "createTask.php";
    
    TaskServiceOp = {};

    TaskServiceOp.getTaskList = function() {
        return $http.get(taskListUrl);
    };

    TaskServiceOp.createTask = function(task) {
        return $http.post(createTaskUrl, task);
    }

    return TaskServiceOp;
}]);

var TaskApp = angular.module('TaskApp', ['TaskService']);

TaskApp.controller('TaskController', function($scope, TaskServiceOp) {
    $scope.taskList = {};
    $scope.event = {};

    $scope.event.getTaskList = function() {
        TaskServiceOp.getTaskList()
            .success(function(taskList) {
                $scope.taskList = taskList;
            })
            .error(function(error) {
                $('#error-modal-body').empty();
                $('#error-modal-body').html('There was an error retrieving data; the server may not be responding');

                $('#error-modal').modal('show');
            });
    };

    $scope.event.createTask = function() {
        var name = $('#task-name').val();
        var date = $('#task-date').val();
        var interval = $('.task-interval:checked').val();

        var task = {
            'task-name' : name,
            'task-date' : date,
            'task-interval' : interval
        };

        TaskServiceOp.createTask(task)
            .success(function(taskData) {
                $('#edit-task-modal').modal('hide');

                if (taskData.error) {
                    $('#error-modal-body').empty();
                    $('#error-modal-body').html(taskData.message);

                    $('#error-modal').modal('show');
                } else {
                    $scope.event.getTaskList();
                }
            })
            .error(function(error) {
                $('#edit-task-modal').modal('hide');

                $('#error-modal-body').empty();
                $('#error-modal-body').html('There was an error posting data; the server may not be responding');

                $('#error-modal').modal('show');
            });
    };

    $scope.event.showCreateModal = function(event) {
        var createModal = $('#edit-task-modal');

        createModal.modal('show');
    };

    $scope.event.getTaskList();
});

/**
 *  Non-angular functions and initializers go here
 */
var createTaskInit = function() {
    var datePicker = $('#task-date-time-picker');

    datePicker.datetimepicker({
        format : 'YYYY-MM-DD hh:mm:ss',
        defaultDate : new Date()
    });
};

$(document).ready(function() {
    createTaskInit();
});
