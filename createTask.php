<?php

include 'config.php';
include 'lib.php';

// Only POST is supported in this script
if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    error('Incorrect HTTP method: only POST is supported');
}

// Initialize our database and a few variables
$mysqli = connect($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);

// Angular posts data in JSON format, so we have to unparse that manually
$postDataString = file_get_contents("php://input");
$postData = json_decode($postDataString, true);

// Sanitize the variables for safety
$taskName = $mysqli->escape_string($postData['task-name']);
$taskDate = $mysqli->escape_string($postData['task-date']);
$taskInterval = $mysqli->escape_string($postData['task-interval']);

// Sanity-checking certain fields
if (empty($taskName) || empty($taskDate) || empty($taskInterval)) {
    error('Some required fields are missing');
}

if (!in_array($taskInterval, array('year', 'month', 'week', 'day', 'hour'))) {
    error('Interval field must be set to a valid value');
}

if (!preg_match('/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/', $taskDate)) {
    error('Date format must be of the form YYYY-MM-DD hh:mm:ss');
}

// Now perform the actual insert
$taskTableInsertQuery = "
    INSERT INTO `task` (`name`, `date`, `interval`)
        VALUES ('{$taskName}', '{$taskDate}', '{$taskInterval}');
";

$result = query($mysqli, $taskTableInsertQuery);

if (false === $result) {
    error('The data was valid but the entry could not be created in the task table');
}

$taskData = array(
    'error' => false,
    'name' => $taskName,
    'date' => $taskDate,
    'interval' => $taskInterval,
);

json($taskData);
