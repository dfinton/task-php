# README #

After pulling the data, you'll preferably want a working instance of MySQL running on localhost. Login details for the MySQL instance are stored in the file "config.php" in the root folder. Verify that these match your local login details and modify if necessary.

Next, you'll want to create a "task_php" database in your MySQL instance. If you changed the $dbName parameter in the config.php file, you'll want to make sure that matches the name of the database you have created.

After setting up the database, you'll need to run the "init.php" script as follows:

```
$ php init.php
```

That will set up the task table and seed the table with some initial values. Once that is done, you should be able to load up the site. **PLEASE NOTE:** Make sure that if you need to set up an .htaccess file, be sure to do so as one is not provided with the repository (I was able to run the site locally on my system without an .htaccess, but your site may differ from mine in that regard). The access point is index.php, so if you installed the site under the document root in a folder called 'task' on localhost, then the URL to access will be:

http://localhost/task

You'll see a list of tasks and a button to create a new task.

The files that were created and written by me are as follows:

```
 config.php
 createTask.php
 getTask.php
 index.php
 init.php
 js/app.js
 css/app.css
 lib.php

```