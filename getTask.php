<?php

include 'config.php';
include 'lib.php';

$emptyResult = array();

// Only GET is supported in this script
if ('GET' !== $_SERVER['REQUEST_METHOD']) {
    json($emptyResult);
}

// Initialize our database and a few variables (sanitizing our inputs!)
$mysqli = connect($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);
$taskId = $mysqli->escape_string($_GET['id']);

// Sanity check our GET parameter (it must either be empty or numeric)
if (!(empty($taskId) || is_numeric($taskId))) {
    json($emptyResult);
}

// Query the database (if an id parameter was supplied, attach a WHERE clause at the end)
$selectTaskTableQuery = "
    SELECT `id`, `name`, `date`, `interval` FROM `task`
";

if (!empty($taskId)) {
    $selectTaskTableQuery .= "WHERE `id` = '{$taskId}'";
}

$resultSet = query($mysqli, $selectTaskTableQuery);

// If we got 0 results, we just exit here with an empty set
if ($resultSet->num_rows === 0) {
    json($emptyResult);
}

// Gather the results into an array. If an id was supplied, we return what should be the only
// item in that array. Otherwise, we return all results that were found
$resultData = array();

while ($result = $resultSet->fetch_assoc()) {
    $date = $result['date'];
    $interval = $result['interval'];

    $taskData = array(
        'id' => $result['id'],
        'name' => $result['name'],
        'date' => $date,
        'interval' => $interval,
    );

    $dueDate = date('Y-m-d H:i:s', strtotime("{$date} +1 {$interval}"));
    $taskData['dueDate'] = $dueDate;

    $resultData[] = $taskData;
}

if (empty($taskId)) {
    json($resultData);
} else {
    json(reset($resultData));
}
